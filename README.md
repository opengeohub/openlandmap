# OpenLandMap

OpenLandMap are data (global land mass), services and web-apps providing access and interactive visualizations of the TB of high resolution data (1 km, 250 m resolution or better) produced by the OpenGeoHub Foundation and contributing organizations.